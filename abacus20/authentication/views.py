import json
from pymongo import MongoClient
from random import randint
import secrets
from pytz import timezone    
from instamojo_wrapper import Instamojo
from datetime import datetime
from collections import namedtuple
from .forms import *
from django.http import JsonResponse
from django.core.mail import BadHeaderError, send_mail
from django.contrib.auth.models import User

config = json.loads(open('./authentication/config.json').read())
india = timezone("Asia/Kolkata")
accommodation_config = config['accommodation']
instamojo_config = config['instamojo']
instamojo = Instamojo(**instamojo_config['connection'])

mongo = config['mongo']
client = MongoClient(**mongo['connection'])
db = client[mongo['db']]

col_users = db['users']
col_referrals = db['referrals']
col_tokens = db['tokens']
col_workshops = db['workshops']
col_workshop_registrations = db['workshop_registrations']
col_payment_requests = db['payment_requests']
col_payments = db['payments']
col_accommodations = db['accommodation']


def generate_token():
    tokens = col_tokens.find({
        "expired": "Y"
    })
    tokens = set(map(lambda x: x['token'], tokens))
    new_token = secrets.token_hex(16)
    while new_token in tokens:
        new_token = secrets.token_hex(16)
    return new_token


def token(email):
    now = datetime.now()
    result = col_tokens.find({
        "email": email,
        "expired": 'N'
    })
    result = list(result)
    if len(result) == 0:
        new_token = generate_token()
        col_tokens.insert_one({
            "email": email,
            "token": new_token,
            "from": str(now),
            "to": "",
            "expired": "N"
        })
        return new_token
    else:
        t = result[0]['from']
        diff = now - datetime.strptime(t, '%Y-%m-%d %H:%M:%S.%f')
        if diff.days >= 1:
            new_token = generate_token()
            col_tokens.update({
                "email": email,
                "expired": 'N'
            }, {
                "$set": {
                    "email": email,
                    "expired": 'Y',
                    "from": result[0]['from'],
                    "to": str(now)
                }
            })
            col_tokens.insert_one({
                "email": email,
                "expired": "N",
                "from": str(now),
                "to": "",
                "token": new_token
            })
            return new_token
        else:
            col_tokens.update({
                "email": email,
                "expired": 'N'
            }, {
                "$set": {
                    "email": email,
                    "expired": 'N',
                    "from": str(now),
                    "to": ""
                }
            })
            return result[0]['token']


def user_exists(data):
    result = col_users.find_one({
        "$or": [{"email": data["email"]}, {"phone_number": data["phone"]}]
    })
    if result is None:
        return False
    else:
        return True


def create_user(data):
    col_users.insert_one(data)
    referral_codes = set(map(lambda x: x['referral_code'], list(col_referrals.find())))
    referral_code = data['email'][:4] + str(randint(1000, 9999))
    while referral_code in referral_codes:
        referral_code = data['email'][:4] + str(randint(1000, 9999))
    col_referrals.insert_one({
        "email": data['email'],
        "referral_code": referral_code,
        "referrals": 0
    })
    return referral_code


def signup(request):
    if request.method == 'POST':
        form = SignUp(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            if user_exists(data):
                return JsonResponse({
                    "result-code": 300,
                    "message": "user exists already"
                })
            else:
                user_code = create_user(data)
                return JsonResponse({
                    "result-code": 200,
                    "userCode": user_code
                })
        else:
            return JsonResponse({
                "result-code": 300,
                "message": "Invalid form"
            })


def user_verify(data):
    result = col_users.find_one({
        "email": data['email']
    })
    if result['password'] == data['password']:
        return True
    else:
        return False

def user_exist(data):
    result = col_users.find_one({
        "email": data['email']
    })
    if(result is None):
        return False
    else:
        return True    

def get_user(data):
    result = col_users.find_one({
        "email": data['email']
    })
    if result['password'] == data['password']:
        return result
    else:
        return None

def login(request):
    if request.method == 'POST':
        form = Login(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            if user_exist(data):
                if user_verify(data):
                    ref_data = dict(col_referrals.find_one({
                        'email': data['email']
                    }))
                    user = get_user(data)
                    payments = col_payments.find_one({
                        "email" : data["email"],
                        "amount" : 150
                    })
                    registered = payments is not None
                    return JsonResponse({
                        "result-code": 200,
                        "userToken": token(data['email']),
                        "userCode": ref_data['referral_code'],
                        "userName": user["name"],
                        "referrals": ref_data['referrals'],
                        "registered" : registered
                    })
                else:
                    return JsonResponse({
                        "result-code": 300,
                        "message": "wrong password" 
                    })
            else:
                return JsonResponse({
                    "result-code": 300,
                    "message": "user doesn't exist"
                })
        else:
            return JsonResponse({
                "result-code": 300,
                "message": "Invalid form"
            })


def workshop_list(request):
    if request.method == 'POST':
        return JsonResponse({
            'result-code': 200,
            'workshops': list(map(lambda x: {
                "name": x["name"],
                "id": x['id'],
                "amount": x['amount']
            }, list(col_workshops.find())))
        })


def accommodation(request):
    if request.method == "POST":
        form = Accommodation(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            result = col_tokens.find_one({
                "token": data['token'],
                "expired": "N"
            })
            if result is not None:
                email = result['email']
                user_data = col_users.find_one({
                    "email": email
                })
                results = col_accommodations.find({
                    "email": email
                })
                Range = namedtuple('Range', ['start', 'end'])
                r1 = Range(start=datetime.strptime(data['from_date'], '%d-%m-%Y'),
                           end=datetime.strptime(data['to_date'], '%d-%m-%Y'))
                for result in results:
                    r2 = Range(start=datetime.strptime(result['from_date'], '%d-%m-%Y'),
                               end=datetime.strptime(result['to_date'], '%d-%m-%Y'))
                    latest_start = max(r1.start, r2.start)
                    earliest_end = min(r1.end, r2.end)
                    delta = (earliest_end - latest_start).days + 1
                    overlap = max(0, delta)
                    if overlap >= 1:
                        return JsonResponse({
                            "result-code": 300,
                            "message": "overlap with already registered dates"
                        })
                diff = datetime.strptime(data['to_date'], '%d-%m-%Y') - datetime.strptime(data['from_date'], '%d-%m-%Y')
                days = diff.days
                total = days * accommodation_config['amount_per_day']
                response = instamojo.payment_request_create(
                    amount=str(total),
                    purpose="accommodation request",
                    send_email=False,
                    email=email,
                    buyer_name=user_data['name'],
                    phone=user_data['phone'],
                    redirect_url=instamojo_config['redirect_url']
                )
                response['payment_request'].update({
                    "accommodation": {
                        "email": email,
                        "from_date": data['from_date'],
                        "to_date": data['to_date']
                    }
                })
                # result = col_accommodations.
                col_payment_requests.insert_one(response['payment_request'])
                return JsonResponse({
                    "result-code": 200,
                    "redirect_url": response['payment_request']['longurl']
                })
            else:
                return JsonResponse({
                    "result-code": 300,
                    "message": "user token expired or not present"
                })
        else:
            return JsonResponse({
                "result-code": 300,
                "message": "Invalid form"
            })


def workshop_register(request):
    if request.method == 'POST':
        print(dict(request.POST))
        form = WorkshopRegister(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            result = col_tokens.find_one({
                "token": data['userToken'],
                "expired": "N"
            })
            if result is not None:
                email = result['email']
                result = col_workshop_registrations.find_one({
                    "email": email,
                    "workshop_id": data['workshopId']
                })
                if result is None:
                    amount = 500
                    purpose="workshop registration - " + data["workshopName"]
                    if int(data["workshopId"]) == 0:
                        amount = 150
                        purpose = "Event Registration"
                    referrals = col_referrals.find_one({
                        "email": email
                    })
                    if referrals['referrals'] >= 10 and amount==500:
                        amount = 0
                        col_referrals.update_one({
                            "email": email
                        }, {
                            "$set": {
                                "referrals": referrals - 10
                            }
                        })
                    user_data = col_users.find_one({
                        "email": email
                    })
                    response = instamojo.payment_request_create(
                        amount=str(amount),
                        purpose=purpose,
                        send_email=False,
                        email=email,
                        buyer_name=user_data['name'],
                        phone=user_data['phone'],
                        redirect_url=instamojo_config['redirect_url'] + "/" + data["workshopId"] + "/"
                    )
                    if not response['success']:
                        return JsonResponse({
                            "result-code": 300,
                            "message": response['message']
                            # "message": ','.join([','.join(i) for i in response['message']].values())
                        })
                    response['payment_request'].update({
                        "workshop_details": {
                            "email": email,
                            "workshop_id": data['workshopId'],
                            "referral_code": data['referralCode']
                        }
                    })
                    col_payment_requests.insert_one(response['payment_request'])
                    return JsonResponse({
                        "result-code": 200,
                        "redirect_url": response['payment_request']['longurl']
                    })
                else:
                    return JsonResponse({
                        "result-code": 300,
                        "message": "already registered"
                    })
            else:
                return JsonResponse({
                    "result-code": 300,
                    "message": "user token expired or not present"
                })
        else:
            return JsonResponse({
                "result-code": 300,
                "message": "Invalid form"
            })

def get_payments(request):
    if request.method == "POST":
        form = GetPayments(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            result = col_tokens.find_one({
                "token": data["userToken"],
                "expired": "N"
            })
            if result is not None:
                email = result['email']
                payments = col_payments.find({
                    "email" : email
                })
                paymentss = []
                for payment in payments:
                    paym = {"amount" : payment["amount"] , "purpose" : payment["purpose"] , "date" : payment["date"]}
                    paymentss.append(paym)
                payments = paymentss
                return JsonResponse({
                        "result-code": 200,
                        "payments": payments
                    })
            else:
               return JsonResponse({
                    "result-code": 300,
                    "message": "you have been logged out"
                }) 
        else:
            return JsonResponse({
                "result-code": 300,
                "message": "Invalid form"
            })

    

def payment_confirmation(request):
    if request.method == "POST":
        data = dict(request.POST)
        form = PaymentConfirmation(request.POST)
        if form.is_valid():
            data = form.cleaned_data
        payment_request = col_payment_requests.find_one({
            "id": data['payment_request_id']
        })
        purpose = payment_request['purpose']
        if data['payment_status'] == "Credit":
            data["purpose"] = purpose
            data["email"] = payment_request["email"]
            data["date"] =  datetime.now().strftime("%d/%m/%Y")
            data["amount"] = 500
            if payment_request["workshop_details"]["workshop_id"] == "0":
                data["amount"] = 150
            req = col_payments.find_one({
                    "payment_id" : data["payment_id"] 
            })
            if not req:
                col_payments.insert_one(data)
                col_workshop_registrations.insert_one(payment_request['workshop_details'])
                col_referrals.find_one_and_update({
                    "referral_code": payment_request['workshop_details']['referral_code']
                }, {
                    "$inc": {
                        "referrals": 1
                    }
                })
            return JsonResponse({
                    "result-code": 200,
                    "message": data["purpose"] + " success"
            })
        else:
            col_payments.insert_one(data)
            if purpose == "accommodation request":
                payment_request['accommodation'].update({
                    "payment_status": "not paid"
                })
                col_accommodations.insert_one()
            return JsonResponse({
                "result-code": 200,
                "message": "payment status noted"
            })
    else:
        return JsonResponse({
            "result-code": 300,
            "message": "Invalid form"
        })


def change_password(request):
    if request.method == "POST":
        form = ChangePassword(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user_token = data['token']
            result = col_tokens.find_one({
                "token": user_token,
                "expired": "N"
            })
            if result is not None:
                email = result['email']
                user_data = col_users.find_one({
                    "email": email
                })
                if user_data['password'] == data['currentpassword']:
                    col_users.update_one({
                        "email": email
                    }, {
                        "$set": {
                            "password": data['newpassword']
                        }
                    })
                    return JsonResponse({
                        "result-code": 200,
                        "message": "password changed successfully"
                    })
                else:
                    return JsonResponse({
                        "result-code": 300,
                        "message": "password incorrect"
                    })

            else:
                return JsonResponse({
                    "result-code": 301,
                    "message": "user token expired or not found"
                })
        else:
            return JsonResponse({
                "result-code": 300,
                "message": "Invalid form"
            })


def send_new_password(email,newpass):
    data = {
        "subject": "Abacus - Change password",
        "message": "Your new password is {}".format(newpass),
        "from_email": "noreply@abacus.org.in",
        "recipient_list": [email]
    }
    try:
        send_mail(**data)
    except Exception or BadHeaderError:
        return False
    return True


def forgot_password(request):
    if request.method == "POST":
        form = ForgotPassword(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            result = col_users.find_one({
                "email": data['email']
            })
            if result is not None:
                newpass = User.objects.make_random_password()
                col_users.update_one({
                        "email": data["email"]
                    }, {
                        "$set": {
                            "password": newpass
                        }
                    })
                if send_new_password(data['email'],newpass):
                    return JsonResponse({
                        "result-code": 200,
                        "message": "email sent successfully"
                    })
                else:
                    return JsonResponse({
                        "result-code": 300,
                        "message": "could not send mail"
                    })
            else:
                return JsonResponse({
                    "result-code": 301,
                    "message": "user doesn't exist"
                })
        else:
            return JsonResponse({
                "result-code": 300,
                "message": "Invalid form"
            })
