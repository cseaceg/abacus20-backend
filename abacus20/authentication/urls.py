from django.conf.urls import url
from authentication.views import *

urlpatterns = [
    url(r'^login/', login, name='login'),
    url(r'^signup/', signup, name='signup'),
    url(r'^workshopList/', workshop_list, name='workshop_list'),
    url(r'^accommodationRequest/', accommodation, name='accommodation'),
    url(r'^workshopRegister/', workshop_register, name="workshop_registration"),
    url(r'^paymentConfirmation/', payment_confirmation, name="payment_confirmation"),
    url(r'^changePassword/', change_password, name="change_password"),
    url(r'^forgotPassword/', forgot_password, name="forgot_password"),
     url(r'^getPayments/', get_payments, name="get_payments")
]
