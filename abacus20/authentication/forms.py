from django import forms


class SignUp(forms.Form):
    name = forms.CharField()
    year = forms.CharField()
    email = forms.CharField()
    phone = forms.CharField()
    college = forms.CharField()
    department = forms.CharField()
    city = forms.CharField()
    state = forms.CharField()
    password = forms.CharField()


class Login(forms.Form):
    email = forms.EmailField()
    password = forms.CharField()


class Accommodation(forms.Form):
    from_date = forms.CharField()
    to_date = forms.CharField()
    userToken = forms.CharField()


class WorkshopRegister(forms.Form):
    workshopId = forms.CharField()
    userToken = forms.CharField()
    referralCode = forms.CharField()
    workshopName = forms.CharField()
 

class PaymentConfirmation(forms.Form):
    payment_id = forms.CharField()
    payment_status = forms.CharField()
    payment_request_id = forms.CharField()

class ChangePassword(forms.Form):
    token = forms.CharField()
    currentpassword = forms.CharField()
    newpassword = forms.CharField()


class ForgotPassword(forms.Form):
    email = forms.CharField()

class GetPayments(forms.Form):
    userToken = forms.CharField()
